# Credit-transfer

Credit management website
A website which transfers credit from one user to another.

Key features-
1)If the same user is transferring credits to himself, an appropriate message is displayed using sweet alert.
2)If the user does not have enough credits, an appropriate message is displayed using sweet alert.
3)Table to view all transfers that can be accessed from the "View transfer table" page.

Database used: Firebase.google.com
Hosted at:  https://vikastshankar.github.io/creditransfer/
